# Image Cropping Tool

## Installation

Download and unzip this repo in your **C:** directory (there is a *Download ZIP* button above).

Install [opencv](http://opencv.org/) on your computer.

#### For Windows

Click to [download](https://sourceforge.net/projects/opencvlibrary/files/opencv-win/2.4.12/opencv-2.4.12.exe/download) the *.exe* and install to **C:\OpenCv**

#### For Linux/Mac

Click to [download](https://sourceforge.net/projects/opencvlibrary/files/opencv-unix/2.4.11/opencv-2.4.11.zip/download) the *.zip* and unzip to **C:\OpenCv**

## Usage

Place an image in the **AppData\ImageToProcess** directory. 

Launch the app thanks to these 3 lines in your command shell :

~~~~
cd C:\ImageLabeling
Scripts\activate.bat
python launchApp.py
~~~~

The first image shows up. 

Click to draw points around your *ROI* (Region-Of-Interest). Lines come up as well to show you the path you are creating.

Have done a mistake ? Come back by typing ```z```.

When you're done with drawing, crop ! Type ```n``` and save the result to a *.txt* file stored in the **AppData\Result** directory.

Your *ROI* is highlighted :)

At any time, type ```q``` to quit. 

__Tips:__ 

* Type ```z``` several time to come back to the beginning of your *ROI* !

* If you begin a new *ROI*, it will be saved to a new *.txt* file :) just type ```n``` any time you've finished to draw a *ROI*.

* If you place a bunch of images in the **AppData\ImageToProcess** directory, the process loops over them :)

## Debug

If a problem occur with opencv you might have to copy/paste the *cv2.pyd* file from **C:\OpenCv\opencv\build\python\2.7\x86** to **C:\ImageLabeling\Lib\site-packages**.

For other problems contact me :) at pierre.cordier@effiscience.solutions