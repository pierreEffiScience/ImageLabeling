# -*- coding: utf-8 -*-       

import numpy as np
import cv2
import AppLib

class Displayer(AppLib.Manager) :
    '''
    '''
    
    def __init__(self):
        '''
        '''
        AppLib.Manager.__init__(self)
    
    def initializeWindow(self):
        '''
        '''
        
        cv2.namedWindow(self.imageName, cv2.WINDOW_AUTOSIZE)
        cv2.setMouseCallback(self.imageName, self.addPoint)
        
    def closeWindow(self):
        '''
        '''
        if ('q' == chr(self.key & 255) or self.key == -1):
            # reinitialize image history
            self.imageHistory = []
            # reinitialize polygon list
            self.coordinateList = []
            return True
    
    def launchWindow(self):
        '''
        '''
        
        while True :
            cv2.imshow(self.imageName,self.image)
        
            self.key = cv2.waitKey(0)
            
            self.removeLastPoint()
            
            self.newObject()
                
            if self.closeWindow() : 
                break
            
        cv2.destroyAllWindows()