# coding: utf-8

"""
**********
ProjetSaas
**********

    ProjetSaas (pss) is a Python package including python codes
    for the projet Saas de Kos

Using
-----

    Just write in Python

    >>> import AppLib
    >>>

"""

import sys
if (sys.version_info[:2] < (2, 7)) :
    m = "Python version >= 2.7 is required (%d.%d detected)."
    raise ImportError(m % sys.version_info[:2])
del sys

from . import manager
from .manager import *
from . import displayer
from .displayer import *
from . import listener
from .listener import *
from . import cropper
from .cropper import *
