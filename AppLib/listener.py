# -*- coding: utf-8 -*-  

from os.path import join
import numpy as np
import cv2
import AppLib

class Listener(AppLib.Displayer):
    '''
    '''
    
    def __init__(self):
        '''
        '''
        AppLib.Displayer.__init__(self)
        self.coordinateList = []
        self.nbObject = 0
        self.color = (255, 191, 83) #(124, 137, 191)
    
    def addPoint(self, event, x, y, flags, param):
        '''
        '''
        if event == cv2.EVENT_LBUTTONDOWN:

            self.imageHistory.append(self.image.copy())
            
            if not self.coordinateList :
                # Store data
                self.coordinateList = [(x, y)]
                # Draw point
                cv2.circle(self.image, self.coordinateList[-1], radius = 3, color = self.color, thickness=-1, lineType=8, shift=0)
            else :
                self.coordinateList.append((x, y))
                # Draw point
                cv2.circle(self.image, self.coordinateList[-1], radius = 3, color = self.color, thickness=-1, lineType=8, shift=0)
                # Draw line between two last
                cv2.line(self.image, self.coordinateList[-1], self.coordinateList[-2], color = self.color, thickness=2, lineType=8, shift=0)

            cv2.imshow(self.imageName, self.image)
    
    def removeLastPoint(self):
        '''
        '''
        if ('z' == chr(self.key & 255)):
            if self.coordinateList :
                self.image = self.imageHistory.pop()
                self.coordinateList.pop()
    
    def newObject(self):
        '''
        '''
        if ('n' == chr(self.key & 255)):
            # increment number of objects
            self.nbObject += 1
            # Draw polygon
            cv2.fillConvexPoly(self.image, np.array(self.coordinateList), color = self.color, lineType=8, shift=0)
            # Save polygon list of points in a new txt : name of image + increment of an id
            self.export()
            # reinitialize image history
            self.imageHistory = []
            # reinitialize polygon list
            self.coordinateList = []