# -*- coding: utf-8 -*-

from os import listdir
from os.path import abspath, dirname, join, isfile
import numpy as np
import cv2

class Manager():
    '''
    '''
    def __init__(self):
        '''
        '''
        self.baseDir = dirname(dirname(abspath(__file__)))
        self.appData = 'AppData'
        self.imageFolder = 'ImageToProcess'
        self.resultFolder = 'Result'
        self.imagePath = join(self.baseDir, self.appData, self.imageFolder)
        self.resultPath = join(self.baseDir, self.appData, self.resultFolder)
        self.image = None
    
    def readImage(self, imageName) :
        '''
        '''
        self.image = cv2.imread(join(self.imagePath, imageName), cv2.IMREAD_COLOR)
        self.imageHistory = []
        
    def listImage(self):
        '''
        '''
        return [f for f in listdir(self.imagePath) if isfile(join(self.imagePath, f))]
    
    def export(self) :
        with open(join(self.resultPath, '{0} - item number {1}.txt'.format(self.imageName, self.nbObject)),'wb') as file:
            for point in self.coordinateList :
                file.write(str(point) + '\n')
            